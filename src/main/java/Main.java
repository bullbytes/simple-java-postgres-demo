import java.sql.*;
import java.time.LocalDateTime;
import java.util.Optional;

public class Main {

    private static final String CREATED_AT_COLUMN = "created_at";
    private static final String NAME_COLUMN = "name";
    private static final String USERS_TABLE = "users";

    public static void main(String[] args) {
        printJavaRuntimeDetails();
        // See docker-compose.yml for network config and credentials
        String url = "jdbc:postgresql://myPostgres:5432/test_db";
        String user = "postgres";
        String password = "4WXUms893U6j4GE&Hvk3S*hqcqebFgo!vZi";

        Optional<Connection> connectionMaybe = getDbConnection(url, user, password);
        connectionMaybe.ifPresentOrElse(connection -> {
            System.out.println("Established connection");
            try {
                createTable(connection);
                addTestData(connection);
                queryDatabase(connection);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }, () -> System.out.println("Could not establish a connection"));

        System.out.println("End of Java program");
    }

    /**
     * Tries a few times to connect to the database.
     * Sleeps between tries.
     *
     * @return {@link Optional optionally} the {@link Connection} to the database
     */
    private static Optional<Connection> getDbConnection(String url, String user, String password) {
        int curTry = 1;
        final int maxTries = 5;
        final int intervalInSeconds = 5;

        do {
            System.out.println("Try " + curTry + " of " + maxTries + " to connect to database at " + url);
            try {
                var connection = DriverManager.getConnection(url, user, password);
                return Optional.of(connection);
            } catch (SQLException e) {

                System.out.println("Connection error: " + e);

                // Only sleep if this isn't the last try
                if (curTry < maxTries) {
                    System.out.println("Waiting for " + intervalInSeconds + " seconds before next connection try");
                    try {
                        Thread.sleep(intervalInSeconds * 1000);
                    } catch (InterruptedException ex) {
                        System.out.println("Couldn't sleep");
                        throw new RuntimeException(ex);
                    }
                }
            }
        } while (++curTry <= maxTries);
        System.out.println("Giving up after " + maxTries + " tries");
        return Optional.empty();
    }

    /**
     * Creates the {@value USERS_TABLE} database table if it doesn't exist yet.
     */
    private static void createTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();

        statement.executeUpdate("CREATE TABLE IF NOT EXISTS " + USERS_TABLE + " (id SERIAL PRIMARY KEY, " + NAME_COLUMN + " VARCHAR(100), " + CREATED_AT_COLUMN + " TIMESTAMP)");
        statement.close();
    }

    private static void addTestData(Connection connection) throws SQLException {
        insertTestData(connection, "Albert");
        insertTestData(connection, "Becky");
        insertTestData(connection, "Caesar");
    }

    private static void insertTestData(Connection connection, String name) throws SQLException {
        String sql = "INSERT INTO " + USERS_TABLE + " (" + NAME_COLUMN + ", " + CREATED_AT_COLUMN + ") VALUES (?, ?)";

        try (var preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, name);
            preparedStatement.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));

            preparedStatement.executeUpdate();
        }
    }

    private static void queryDatabase(Connection connection) throws SQLException {
        var query = "SELECT * FROM " + USERS_TABLE;
        System.out.printf("Querying database with '%s'\n", query);
        try (var statement = connection.createStatement();
             var resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                System.out.println("Name: " + resultSet.getString(NAME_COLUMN));
                System.out.println("Created at: " + resultSet.getString(CREATED_AT_COLUMN));
                System.out.println("======================================");
            }
        }
    }

    private static void printJavaRuntimeDetails() {
        var runtime = Runtime.getRuntime();

        // Get information about the Java runtime environment
        String javaVersion = System.getProperty("java.version");
        String javaVendor = System.getProperty("java.vendor");
        String javaHome = System.getProperty("java.home");
        // Maximum amount of memory that the JVM will attempt to use
        long maxMemory = runtime.maxMemory();
        // Total amount of memory currently available to the JVM
        long totalMemory = runtime.totalMemory();
        // Amount of memory currently available for new objects
        long freeMemory = runtime.freeMemory();

        System.out.println("Java version: " + javaVersion);
        System.out.println("Java vendor: " + javaVendor);
        System.out.println("Java home: " + javaHome);
        System.out.println("Max memory (bytes): " + maxMemory);
        System.out.println("Total memory (bytes): " + totalMemory);
        System.out.println("Free memory (bytes): " + freeMemory);
    }
}