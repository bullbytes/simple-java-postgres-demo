# simple-java-postgres-demo

A demo application showing how to use Java and Postgres (using a custom Postgres configuration), connected via Docker Compose.

# run.sh
Run the application using `sudo run.sh`. This script creates a `./logs` directory owned by user "postgres" (hence the need for `sudo`). Postgres in the container will write log messages into this directory.

