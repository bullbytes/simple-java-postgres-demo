# This stage only downloads the dependencies but doesn't build the project.
# This layer is cached and will only be rebuilt if the pom.xml file changes.
FROM maven:3.9.6-eclipse-temurin-22 AS build
WORKDIR /app
COPY pom.xml .
# First, download dependencies. Second, copy the source code and build the application. This ensures that the dependencies are cached and only rebuilt if the pom.xml file changes.
# See https://stackoverflow.com/questions/53691781/how-to-cache-maven-dependencies-in-docker
RUN mvn dependency:go-offline -B

COPY src ./src
RUN mvn package

# Run the application. We reuse the fat jar from the build stage.
FROM eclipse-temurin:22-jdk
WORKDIR /app
COPY --from=build /app/target/simple-java-postgres-demo-0.1-SNAPSHOT-jar-with-dependencies.jar app.jar
CMD ["java", "-jar", "app.jar"]
